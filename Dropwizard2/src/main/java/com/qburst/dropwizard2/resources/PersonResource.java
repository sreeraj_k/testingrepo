package com.qburst.dropwizard2.resources;

import com.qburst.dropwizard2.databaseHandling.mysql.PersonDAO;
import com.qburst.dropwizard2.view.PersonView;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by sreeraj on 22/4/15.
 */
@Path("/people/{username}")
@Produces(MediaType.TEXT_HTML)
public class PersonResource {
    private final PersonDAO dao;

    public PersonResource(PersonDAO dao) {
        this.dao = dao;
    }

    @GET
    public PersonView getPerson(@PathParam("username") String username) {
        System.out.println("This is executed");
        return new PersonView(dao.findByUsername(username));
    }
}
