package com.qburst.dropwizard2.resources;

import com.codahale.metrics.annotation.Timed;
import com.google.common.base.Optional;
import com.qburst.dropwizard2.core.Person;
import com.qburst.dropwizard2.core.Saying;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.atomic.AtomicLong;

@Path("/signupNow")
@Produces(MediaType.APPLICATION_JSON)
public class SignupResource {
    private final String template;
    private final String defaultName;
    private final AtomicLong counter;

    public SignupResource(String template, String defaultName) {
        this.template = template;
        this.defaultName = defaultName;
        this.counter = new AtomicLong();
    }

    @GET
    @Timed
    public Person sayHello(@QueryParam("name") Optional<String> name,
                           @QueryParam("username") Optional<String> username,
                           @QueryParam("password") Optional<String> password) {


        final String value = String.format(template, name.or(defaultName));
        return new Person();
    }

}