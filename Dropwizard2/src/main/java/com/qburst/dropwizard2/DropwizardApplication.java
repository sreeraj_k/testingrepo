package com.qburst.dropwizard2;

import com.qburst.dropwizard2.core.Person;
import com.qburst.dropwizard2.databaseHandling.mysql.PersonDAO;
import com.qburst.dropwizard2.resources.PersonResource;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import org.hibernate.sql.Template;

import java.util.Map;

/**
 * Created by sreeraj on 22/4/15.
 */
public class DropwizardApplication extends Application<DropwizardConfiguration> {
    public static void main(String[] args) throws Exception {
        new DropwizardApplication().run(args);
    }

    private final HibernateBundle<DropwizardConfiguration> hibernateBundle =
            new HibernateBundle<DropwizardConfiguration>(Person.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(DropwizardConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };



    @Override
    public String getName() {
        return "hello-world";
    }

    @Override
    public void initialize(Bootstrap<DropwizardConfiguration> bootstrap) {
        // nothing to do yet
        bootstrap.addBundle(new ViewBundle<DropwizardConfiguration>(){
            @Override
            public Map<String, Map<String, String>> getViewConfiguration(DropwizardConfiguration config) {
                return config.getViewRendererConfiguration();
            }
        });
        bootstrap.addBundle(hibernateBundle);
    }

    @Override
    public void run(DropwizardConfiguration configuration,
                    Environment environment) {


        final PersonDAO dao = new PersonDAO(hibernateBundle.getSessionFactory());

        final PersonResource resource = new PersonResource(dao);
        environment.jersey().register(resource);


    }

}