package com.qburst.dropwizard2.view;

import com.qburst.dropwizard2.core.Person;
import io.dropwizard.views.View;

/**
 * Created by sreeraj on 22/4/15.
 */
public class PersonView extends View {

    private final Person person;

    public PersonView(Person person) {
        super("person.ftl");
        this.person = person;
    }

    public enum Template{
        FREEMARKER("person.ftl"),
        MUSTACHE("mustache/person.mustache");

        private String templateName;
        private Template(String templateName){
            this.templateName = templateName;
        }

        public String getTemplateName(){
            return templateName;
        }
    }

    public Person getPerson() {
        return person;
    }

    public PersonView(PersonView.Template template, Person person) {
        super(template.getTemplateName());
        this.person = person;
    }
}
