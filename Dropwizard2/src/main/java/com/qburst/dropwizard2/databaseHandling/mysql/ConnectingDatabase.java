package com.qburst.dropwizard2.databaseHandling.mysql;

import java.sql.*;

/**
 * Created by sreeraj on 22/4/15.
 */
public class ConnectingDatabase {

    static String dbUrl = "jdbc:mysql://localhost/dropwizard2";
    static String dbClass = "com.mysql.jdbc.Driver";
    static String query = "Show tables";
    static String dbName = "dropwizard2";
    static String usernameDatabase = "root";
    static String passwordDatabase = "itsmyown";

    private Connection connection;
    private PreparedStatement signupDetails;
    private PreparedStatement checkLogin;
    private Statement statement;

    public ConnectingDatabase() {

        init();
    }

    public void init() {
        try {

            Class.forName(dbClass);
            connection = DriverManager.getConnection(dbUrl,
                    usernameDatabase, passwordDatabase);
            statement = connection.createStatement();
            String insertSignup = "INSERT INTO " + dbName + ".login values(?,?,?)";
            String countLogin = "Select user from " + dbName + ".login where username = ? AND password = ? ";
            checkLogin = connection.prepareStatement(countLogin);
            signupDetails = connection.prepareStatement(insertSignup);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String arg[]) {

        ConnectingDatabase con = new ConnectingDatabase();

        con.checkLogin("sreeraj","pass");
    }

    public void testConnect() {

        try {


            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String tableName = resultSet.getString(1);
                System.out.println("Table name : " + tableName);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean insertSignupDetails(String user, String username, String password) {


        try {
            signupDetails.setString(1, user);
            signupDetails.setString(2, username);
            signupDetails.setString(3, password);

            signupDetails.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;


    }

    public boolean checkLogin(String username, String password) {
        int count = 0;
        try {
            checkLogin.setString(1, username);
            checkLogin.setString(2, password);
            ResultSet rs = checkLogin.executeQuery();

            while (rs.next()) {
                count++;
                if (count > 1) {
                    throw new Exception("Attack Found");
                }
                String user = rs.getString("user");
                System.out.println("user " + user);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            return false;
        }finally {
            if(count == 1){
                return true;
            }
            else{
                return false;
            }
        }

    }

}
