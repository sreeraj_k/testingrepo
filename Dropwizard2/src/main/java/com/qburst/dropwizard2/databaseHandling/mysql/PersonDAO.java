package com.qburst.dropwizard2.databaseHandling.mysql;

import com.google.common.base.Optional;
import com.qburst.dropwizard2.core.Person;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;


/**
 * Created by sreeraj on 22/4/15.
 */
public class PersonDAO extends AbstractDAO<Person> {
    public PersonDAO(SessionFactory factory) {
        super(factory);
    }

    public Optional<Person> findUsername(String username) {
        return Optional.fromNullable(get(username));
    }


    public Person create(Person person) {
        return persist(person);
    }

    public List<Person> findAll() {
        return list(namedQuery("com.qburst.dropwizard2.core.Person.findAll"));
    }
    public Person findByUsername(String username){
        List<Person> l;
        l =  list(namedQuery("com.qburst.dropwizard2.core.Person.findByUsername"));
        return l.get(0);
    }
}