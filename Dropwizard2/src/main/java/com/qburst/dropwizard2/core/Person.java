package com.qburst.dropwizard2.core;


import javax.persistence.*;

/**
 * Created by sreeraj on 22/4/15.
 */

@Entity
@Table(name = "login")
@NamedQueries({
        @NamedQuery(
                name = "com.qburst.dropwizard2.core.Person.findAll",
                query = "SELECT p FROM login p"
        ),
        @NamedQuery(
                name = "com.qburst.dropwizard2.core.Person.findByUsername",
                query = "SELECT p FROM Person p WHERE p.username = :givenusername "
        ),
        @NamedQuery(
        name="com.qburst.dropwizard2.core.Person.findByUserNameAndPassword",
        query = "SELECT p FROM Person p WHERE p.username = :givenusername AND p.password = :givenpassword"
        )
})
public class Person {

    public Person(){


    }

    @Column(name = "name", nullable = false)
    private String name;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

}
